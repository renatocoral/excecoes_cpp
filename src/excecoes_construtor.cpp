#include <cstdio>
#include <string>
#include <exception>

using namespace std;

static string unk = "desconhecido";
static string clone_prefix = "clone-";

class E : public exception {
    const char * msg = nullptr;
    E(){};
public:
    E(const char * s) throw() : msg(s) {}
    const char * what() const throw() { return msg; }
};

class Animal {
    string _tipo;
    string _nome;
    string _som;
public:
    Animal();   // constructor padrão
    Animal(const string & tipo, const string & nome, const string & som);
    
    Animal(const Animal &); // constructor de cópia
    Animal & operator=(const Animal &); // copy operator
    ~Animal();  // destrutor
    void imprime() const;
};

// -- Implementação --
Animal::Animal() : _tipo(unk), _nome(unk), _som(unk) {
    puts("Construtor Padrao");
}

Animal::Animal(const string & type, const string & nome, const string & sound)
: _tipo(type), _nome(nome), _som(sound) {
    puts("Construtor com Argumentos");
    if(type.length() == 0 || nome.length() == 0 || sound.length() == 0) {
        throw E("Parametros Insuficientes");
    }
}

Animal::Animal(const Animal & a) {
    puts("Construtor de Copia");
    _nome = clone_prefix + a._nome;
    _tipo = a._tipo;
    _som = a._som;
}

Animal::~Animal() {
    printf("Destrutor: %s o %s\n", _nome.c_str(), _tipo.c_str());
}

void Animal::imprime () const {
    printf("%s, o %s, %s\n", _nome.c_str(), _tipo.c_str(), _som.c_str());
}

Animal & Animal::operator=(const Animal & o) {
    puts("Operador designacao");
    if(this != &o) {
        _nome = clone_prefix + o._nome;
        _tipo = o._tipo;
        _som = o._som;
    }
    return *this;
}

int main( int argc, char ** argv ) {
    try {
        Animal umAnimal("Cachorro", "Rex", "Late");
        Animal outroAnimal("Gato", "Felix", "");
        umAnimal.imprime();
        outroAnimal.imprime();
    } catch ( exception & e ) {
        printf("Erro: %s\n", e.what());
    }
    
    return 0;
}









